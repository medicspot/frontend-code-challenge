# Frontend code challenge

This challenge has two parts:

1. Written answers to [Questions](./QUESTIONS.md)
2. Build a form to search and display results

We're looking for elegant, clean solutions.  Try to think of and handle possible edge cases.

This challenge is sent to experienced developers and newcomers alike. Developers who are familiar with the technologies can complete this within an hour, those who are less familiar will take longer. We suggest spending a maximum of 2 hours on this challenge, the objective is to demonstrate you think through a problem.

There are [hints](./HINTS.md) available should you get stuck.

## Part 1: Questions

The goal here is to describe as clearly and tersely as possible. As much an assesment of communications as JavaScript knowledge.

## Part 2: The search form

The objective is to provide a user interface to search for names.

The interface should comprise of a search box and list of results.

When the user starts typing the results should be displayed in the list below.

```
   [ Search here     ]

   * Result 1
   * Result 2
   * Result 3
   * Result 4

```

NOTE: It is important to display the correct results for a given search term.

You are provided with the endpoint

    /locations?q=fuzzyMatchString

Where `fuzzyMatchString` will be a partial name of a location.

You will receive a JSON response with fields `geonameid`, `name`, `latitude` and `longitude`

Implementation:

The implementation should be a single page app.  Feel free to use any tools or frameworks to create the solution. The solution is only required to support the latest version of Chrome.

## Rules

All work should be commited into a fork of this repo. Please note you won't have permission to push to this repo directly (see <https://help.github.com/articles/fork-a-repo> for help)

You'll get bonus points if you

 * Display coordinates next to results
 * Unit test your code
 * Provide good documentation
 * Sort the results by the closest name match

Good luck!
