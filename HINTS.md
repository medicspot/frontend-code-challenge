# Challenge 1: Questions

Depending on experience you might want to run the scripts and debug the statements to give a confident answer.

# Challenge 2: The search form

The search app can be built using the tools of your choice. Building with vanilla JS is fine or you may wish to import UI tooling such as React using something like unpkg.

```html
<script src="https://unpkg.com/react@16/umd/react.development.js" crossorigin></script>
<script src="https://unpkg.com/react-dom@16/umd/react-dom.development.js" crossorigin></script>
```

There is a starter `/public/index.html` file for the client app. This is served from the webserver.
