const sqlite3 = require('sqlite3');
const express = require('express');
const { open } = require('sqlite');

const minimumQueryLength = 2;

let closing = false;

const fetchQuery = `
  SELECT geonameid, name, latitude, longitude
  FROM locations
  WHERE name LIKE @queryString
`;

(async () => {
  const app = express();
  const port = 3000;
  const db = await open({
    filename: "./data/geo.db",
    driver: sqlite3.cached.Database
  })

  // Prepare sqlite query
  const fetch = await db.prepare(fetchQuery);

  // Service /public folder as static content
  app.use(express.static("public"));

  // API endpoint for /locations?q=matchString
  app.get("/locations", async (req, res) => {
    res.setHeader("Content-Type", "application/json; charset=utf-8");
    res.setHeader("Access-Control-Allow-Origin", "*");

    if (req.query.q.length >= minimumQueryLength) {
      const results = await fetch.all({
        "@queryString": `%${req.query.q}%`,
      });
      res.json(results);
    } else {
      res.status(422).send(`The queryString must be at least length of ${minimumQueryLength}`);
    }
  });

  //Start the server
  app.listen(port, async () => {
    console.info(`Geo search app listening at http://localhost:${port}`);
  })

  //Close the database before quitting
  async function closeConnections() {
    if (!closing) {
      console.info("\nClosing connection…");

      closing = true;

      try {
        await fetch.finalize();
        await db.close();
      } catch (e) {
        console.warn(e.message);
      } finally {
        process.exit();
      }
    }
  }

  process.on("SIGINT", closeConnections);
})();
